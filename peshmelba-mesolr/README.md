# singularity-peshmelba-mpich-3.1.4

Contenairisation avec singularity de l'application PESHMELBA pour Emilie Rouzies pour le meso@LR

## BUILD
```bash
singularity build --sandbox peshmelba-mpichv-3.1.4.sif peshmelba-mpichv-3.1.4.def
```

## USAGE
Where $SCRATCH is directory with write-access
```bash
singularity exec --writable --bind $SCRATCH/INPUT1/:/opt/INPUT,$SCRATCH/OUTPUT1:/opt/OUTPUT  peshmelba-mpichv-3.1.4.sif bash -c "cd /opt/PESHMELBA && ./run_mpi1.sh"
```

## USAGE on MESO@LR
Don't forget to adapt the file to your needs
```bash
sbatch slurm-peshmelba-singularity.sh
```
