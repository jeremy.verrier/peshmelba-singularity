#! /usr/bin/env bash

# STEP 2 : Make.include sur PESHMELBA
cd /opt
tar xvzf /opt/PESHMELBA.tar.gz
cp /opt/Make.include_mpi1 /opt/PESHMELBA/Make.include

# STEP 3 : Changement des entêtes des fichiers "main_*.py"

cd /opt/PESHMELBA
sed -i 's:#!/nfs/projets/peshmelba/chantilly/bin/python2.7:#!/opt/miniconda/bin/python:' main_*.py

# Cleaning ....
cd ROSS03
make clean
cd ..
make clean

# Tuning
sed -i 's:folder = "/home/emilie.rouzies/Documents/Polldiff/projet/donnees_modeles/OpenPALM/PESHMELBA_V5.1/":folder = "/opt/":' /opt/PESHMELBA/config_param.py
sed -i 's:/home/emilie.rouzies/Documents/Polldiff/projet/donnees_modeles/OpenPALM/PESHMELBA_V5.1/:/opt/:' /opt/PESHMELBA/config_param.txt

# make de PALM
make

