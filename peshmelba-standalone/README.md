# singularity-peshmelba-standalone

Contenairisation avec singularity de l'application PESHMELBA pour Emilie Rouzies
* Un dossier peshmelba-standalone qui fonctionnera sur toutes machines avec singularity installé
* Un dossier spécifique pour faire tourner l'application sur le MESO@LR

## BUILD
```bash
singularity build --sandbox peshmelba-standalone.sif peshmelba-standalone.def
```

## USAGE
Where $SCRATCH is directory with write-access
```bash
singularity exec --writable --bind $SCRATCH/INPUT1/:/opt/INPUT,$SCRATCH/OUTPUT1:/opt/OUTPUT  peshmelba-standalone.sif bash -c "cd /opt/PESHMELBA && ./run_mpi1.sh"
```

